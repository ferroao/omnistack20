const express = require('express');
const mongoose = require('mongoose')
const routes = require('./routes')
const app = express();

mongoose.connect('mongodb+srv://froao2020:ik1W94hg00DndbuN@cluster0-s17ph.mongodb.net/test?retryWrites=true&w=majority', {
//mongoose.connect('mongodb+srv://tom:week12345678@cluster0-lkwxl.mongodb.net/week10?retryWrites=true&w=majority', {

	useNewUrlParser: true,
	useUnifiedTopology: true
});
// necessary for express to understand request
// format json

// localhost:3333

// principal methods http
// GET - I want to receive an info
// POST - I want to create an info, like save 
//, PUT- I want to edit, 
// DELETE - delete

// tipos de parametros: (dentro do express)
// Query params: son utilizados no metodo GET
// se usa para filtrar, como busqueda

// in insomnia they appear under the upper part.
// i.e. search Diego, in New Name (field), New Value (field)
// under Query
// that appears in the new URL
// Query Params: request.query (Filtros, ordenação, paginação)

// app.post('/users/', (request, response) => {
//	  console.log(request.query)

// Route params: usados nos metodos PUT and DELETE
// request.params (identificar um recurso na alteracao ou remocao)
// app.delete('/users/:id', (request, response) => {
	//  console.log(request.params)
// to test this in insomnia, use DELETE and url
// localhost:3333/users/1

// Body: most used with POST, and PUT (insomnia)
//app.post('/users', (request, response) => {
//	  console.log(request.body)	
//	  return response.json({ message: 'Hello Omnis '}) 
//	});

// MongoDB (nao-relacional)
// no es muy bueno para productos, es complicado de escalar

// express.json needs to be before routes
app.use(express.json())
app.use(routes)
app.listen(3333);

// pasta models denro de src
// son representaciones de entidades