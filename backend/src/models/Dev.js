const mongoose = require('mongoose')
const PointSchema = require('./utils/PointSchema')
// necesario porque necesitamos informar cual es el formato
// dentro de la base de datos, ex, nome , descripcion, email

const DevSchema = new mongoose.Schema({
	name: String,
	github_username: String,
	bio: String,
	avatar_url: String,
	// this is an array, several techs
	techs: [String],
	location: {
		type: PointSchema,
		index: '2dsphere'
	}
})
// Dev = nome com o que vai ser salvo no database
module.exports = mongoose.model('Dev', DevSchema)