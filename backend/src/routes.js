const { Router } = require('express');
const axios = require('axios')
const Dev = require('./models/Dev')
const routes = Router();
// async and await needed for waiting api in internet

routes.post('/devs', async (request, response) => {
	// console.log for making debug	
	//console.log(request.body)	
	const { github_username, techs, latitude, longitude } = request.body
// crase allows to use variables
	const apiResponse = await axios.get(`https://api.github.com/users/${github_username}`)
	//console.log(apiResponse.data)
// name = login equivalent to if below ( use let instead const)
	const { name = login, avatar_url, bio } = apiResponse.data;
	// if name not exist, use login
	//if (!name) {
	//	name = apiResponse.data.login
	//}
	// map recorrer todas as techs and trim
	// need to transform string in insomnia to vector as in Dev.js
	const techsArray = techs.split(',').map(tech => tech.trim())
	// when name of variable = propiedade not necessary : some
	// short syntax

	const location = {
		type = 'Point',
		coordinates: [longitude, latitude],
	}

    const dev = await Dev.create({
		github_username,
	 	name,
	 	avatar_url,
	 	bio,
		techs: techsArray, 
		location  
	})

	//	console.log(name, avatar_url, bio, github_username)
	return response.json(dev)
	//return response.json({ message: 'Hello Omnis2 '}) 
	});

module.exports = routes;